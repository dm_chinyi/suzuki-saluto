import smoothscroll from 'smoothscroll-polyfill';

const toggleMenu = () => {
  const menu = document.getElementById('js-menu-btn');
  const body = document.body;
  const menuTitle = document.querySelector('.menu-block__trigger-text');

  menu.addEventListener('click', () => {
    const menuIsOpen = body.classList.contains('is-open');

    if (!menuIsOpen) {
      body.classList.add('is-open');
      menuTitle.textContent = 'close';
    } else {
      body.classList.remove('is-open');
      menuTitle.textContent = 'menu';
    }
  })
}

const scrollSmoothToSection = () => {
  const navLinks = [...document.querySelectorAll('.nav__link')];
  const body = document.body;
  const header = document.querySelector('.header');
  const offset = 2;

  navLinks.forEach(link => {
    link.addEventListener('click', (e) => {
      // 有外部連結話，就直接另開視窗連外
      if (e.target.hash.indexOf('#') === -1) {
        body.classList.remove('is-open');
        return;
      }

      e.preventDefault();

      const target = document.querySelector(e.target.hash);
      const scrollDistance = target.getBoundingClientRect().top + window.pageYOffset - header.offsetHeight + offset;

      // 否則直接滑到指定區塊
      window.scrollTo({
        top: scrollDistance,
        behavior: 'smooth',
      })

      body.classList.remove('is-open');
    })
  })
}

// 解決打開 Menu overlay 後，當小筆電的頁面過長出現捲軸時，捲動時內容會壓在 Close Button 上
const scrollMenuTitle = () => {
  const menubar = document.getElementById('menubar');
  const overlays = Array.from(document.querySelectorAll('.js-overlay'));

  menubar.style.top = 0;

  overlays.forEach(overlay => {
    overlay.addEventListener('scroll', () => {
      menubar.style.top = `${-overlay.scrollTop}px`;
    })
  })
}

const handleMenu = () => {
  toggleMenu();
  scrollSmoothToSection();
  scrollMenuTitle();
  smoothscroll.polyfill();
}

export default handleMenu;
