import ScrollMagic from "scrollmagic";
import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';

const slideInOnScroll = () => {
  const controller = new ScrollMagic.Controller();

  new ScrollMagic.Scene({
      triggerElement: '.hero',
      triggerHook: 0.7,
    })
    .setClassToggle('.hero', 'appear')
    .reverse(false)
    .addTo(controller)

  new ScrollMagic.Scene({
      triggerElement: '.mix',
      triggerHook: 0.8,
    })
    .setClassToggle('.mix', 'appear')
    .reverse(false)
    .addTo(controller)

  new ScrollMagic.Scene({
      triggerElement: '.ride__cover',
      triggerHook: 1,
    })
    .setClassToggle('.ride__cover', 'appear')
    .reverse(false)
    .addTo(controller)

  new ScrollMagic.Scene({
      triggerElement: '.ride__content',
      triggerHook: 0.8,
    })
    .setClassToggle('.ride__content', 'appear')
    .reverse(false)
    .addTo(controller)

  new ScrollMagic.Scene({
      triggerElement: '.key',
      triggerHook: 0.7,
    })
    .setClassToggle('.key', 'appear')
    .reverse(false)
    .addTo(controller)

  new ScrollMagic.Scene({
      triggerElement: '.seat',
      triggerHook: 0.7,
    })
    .setClassToggle('.seat', 'appear')
    .reverse(false)
    .addTo(controller)

  new ScrollMagic.Scene({
      triggerElement: '.seat__heading',
      triggerHook: 0.6,
      duration: window.innerHeight,
    })
    .setClassToggle('.seat__heading', 'scrolling')
    .addTo(controller)
    .setTween('.seat__heading', 1, {
      y: 180,
      ease: 'ease-in-out'
    })

  const cards = Array.from(document.querySelectorAll('.card'));
  cards.forEach(card => {
    new ScrollMagic.Scene({
        triggerElement: card,
        triggerHook: 1,
      })
      .setClassToggle(card, 'appear')
      .reverse(false)
      .addTo(controller)
  })

  new ScrollMagic.Scene({
      triggerElement: '.engine',
      triggerHook: 0.7,
    })
    .setClassToggle('.engine', 'appear')
    .reverse(false)
    .addTo(controller)
}

const setDesignerSectionFixed = () => {
  const headerElement = document.querySelector('.header');
  const designerElement = document.querySelector('.designer');
  const specElement = document.querySelector('.spec');

  const isAlreadyFixed = (element) => {
    return 'fixed' === window.getComputedStyle(element, null).getPropertyValue('position');
  }

  const getSpecOffsetSuffixPx = () => {
    return (window.innerHeight < designerElement.clientHeight ?
        window.innerHeight :
        designerElement.clientHeight) +
      'px';
  }

  const getHeaderOffsetHeight = () => {
    return isAlreadyFixed(headerElement) ?
      headerElement.clientHeight :
      0;
  }

  const isDesignerScrollToHeaderBottom = () => {
    return designerElement.offsetTop -
      getHeaderOffsetHeight() -
      getWindowScrollTop() <= 0;
  }

  const isSpecScrollOutDesigner = () => {
    return specElement.getBoundingClientRect().top -
      getHeaderOffsetHeight() -
      (
        designerElement.getBoundingClientRect().top + designerElement.getBoundingClientRect().height
      ) >= 0;
  }

  const getWindowScrollTop = () => {
    return Math.max(
      window.pageYOffset,
      document.documentElement.scrollTop,
      document.body.scrollTop
    );
  }

  const original_margin_top = specElement.style.marginTop;

  window.addEventListener('scroll', function () {

    // fixed
    if (isDesignerScrollToHeaderBottom() && !isAlreadyFixed(designerElement)) {
      designerElement.style.position = 'fixed';
      designerElement.style.zIndex = -1;
      designerElement.style.top = getHeaderOffsetHeight() + 'px';

      specElement.style.marginTop = getSpecOffsetSuffixPx();
    }

    // restore
    if (isAlreadyFixed(designerElement) && isSpecScrollOutDesigner()) {
      specElement.style.marginTop = original_margin_top;

      designerElement.style.position = 'static';
      designerElement.style.zIndex = 1;
      designerElement.style.top = 'auto';
    }

  }); //^ addEventListener scroll
}

const handleScrollAnimation = () => {
  slideInOnScroll();
  setDesignerSectionFixed();
}

export default handleScrollAnimation;
