import preloadImages from './js/components/preload';
import handleMenu from './js/components/menu';
import './styles/index.scss';

preloadImages();
handleMenu();
