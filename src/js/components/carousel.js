import Glide from '@glidejs/glide'
import '../../styles/plugins/glide.core.min.css';
import '../../styles/plugins/glide.theme.min.css';

// GlideJS
// https://glidejs.com/
const runCarousel = () => {
  const glide = new Glide('.glide', {
    type: 'carousel',
    autoplay: 3800,
    animationDuration: 800,
    hoverpause: false,
  })

  glide.mount();
}

export default runCarousel;
