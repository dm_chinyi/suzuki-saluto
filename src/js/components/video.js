const createYoutubeIframe = () => {
  const youtube = document.getElementById('youtube');

  youtube.addEventListener('click', (e) => {
    const wrapper = e.currentTarget;
    const iframe = document.createElement('iframe');
    const videoId = youtube.dataset.embed;
    const url = `https://www.youtube.com/embed/${videoId}?autoplay=1&amp;loop=1&amp;showinfo=0&amp;modestbranding=1&amp;rel=0&amp;playlist=${videoId}`;

    iframe.setAttribute('allow', 'autoplay');
    iframe.setAttribute('allowfullscreen', 1);
    iframe.setAttribute('src', url);
    wrapper.innerHTML = '';
    wrapper.appendChild(iframe);
  })
}

export default createYoutubeIframe;
