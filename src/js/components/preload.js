import createYoutubeIframe from './video';
import handleScrollAnimation from './scroll';
import runCarousel from './carousel';

var counts = 0;

const enterMainPage = () => {
  const loadingScreen = document.querySelector('.loading');
  const mainContent = document.querySelector('.main-content');

  createYoutubeIframe();
  runCarousel();
  loadingScreen.classList.add('closed');
  mainContent.style.visibility = 'visible';
  handleScrollAnimation();
}

var preloadImages = () => {
  var images = document.querySelectorAll('.preload');

  images.forEach(image => {

    if (!image.src) {
      var cssStyle = window.getComputedStyle(image, null) || image.currentStyle;
      var bgUrl = cssStyle.backgroundImage.slice(4, -1).replace(/"/g, "");
    }

    var imageUrl = bgUrl || image.src;
    var newImage = new Image;

    newImage.addEventListener('load', () => {
      counts += 1;

      if (counts === images.length) {
        enterMainPage();
      }
    })

    newImage.src = imageUrl;
  })
}

export default preloadImages;
